const fs = require('fs');
const axios = require('axios');

class Searches {

    historical = [];
    dbPath = './database/data.json';

    constructor() {
        this.readDB();
    }

    get historicalCaptitalizado() {
        return this.historical.map( place => {
            let words = place.split(' ');
            words = words.map( word => word[0].toUpperCase() + word.substring(1) );
            return words.join(' ');
        } );
    }

    get paramsMapbox() {
        return {
            'access_token': process.env.MAPBOX_ACCESS_TOKEN,
            'limit': 10,
            'language': 'en'
        }
    }

    get paramsOpenWeather() {
        return {
            'lang': process.env.OPENWEATHER_LANG,
            'units': process.env.OPENWEATHER_UNITS,
            'appid': process.env.OPENWEATHER_ACCESS_TOKEN
        }
    }

    async city( city = '' ) {

        try {
            // Http requrest
            const instanceCity = axios.create({
                baseURL: `https://api.mapbox.com/geocoding/v5/mapbox.places/${ city }.json`,
                params: this.paramsMapbox
            })
            const cityData = await instanceCity.get();
            return cityData.data.features.map( place => ({
                id: place.id,
                name: place.place_name,
                lng: place.center[0],
                lat: place.center[1]
            }));

        } catch (error) {
            throw error;
        }
    }    

    async weather(lat, lon) {
        try {
            const instanceWeather = await axios.create({
                baseURL: `https://api.openweathermap.org/data/2.5/weather`,
                params: { ...this.paramsOpenWeather, lat, lon }
            })
            const weatherData = await instanceWeather.get();
            const { weather, main } = weatherData.data;
            return { 
                desc: weather[0].description,
                min: main.temp_min,
                max: main.temp_max,
                temp: main.temp
            };
            
        } catch (error) {
            throw error;
        }
    }

    insertHistory( place = '' ) {
        if ( this.historical.includes( place.toLocaleLowerCase() ) ) return;
        
        this.historical.unshift( place.toLocaleLowerCase() );
        this.saveDB();
    }

    saveDB() {
        const payload = {
            historical: this.historical
        }
        fs.writeFileSync( this.dbPath, JSON.stringify( payload ) );
    }

    readDB() {
        if ( !fs.existsSync( this.dbPath )) return;

        const info = fs.readFileSync( this.dbPath, { encoding: 'utf-8' } );
        const data = JSON.parse( info );
        this.historical = data.historical;
    }

}

module.exports = Searches;