# README #

### Weather NodeJS Console APP ###
With this app you can search places and show localization and weather by console. 

## Steps
First that all, clone this repo 
```sh
$ git clone https://jcguerra46@bitbucket.org/jcguerra46/weather-nodejs-console-app.git
```

Copy globals environment file in your main directory
```sh
$ cp .env.example .env
```

Install all dependencies
```sh
$ npm install
```

You need an access token to use the [MapBox Geocoding API](https://www.mapbox.com/), it's free, you just need to create your account, then create the access token and put it in the env file.
The same way, you need an access token to use the [OpenWeather API](https://openweathermap.org/), it's free too, create your account, then create the access token and put it in the env file.

## Run the application
```sh
$ npm start
```

## Author
Juan Carlos Guerra 
```sh
email: juancarlosguerra46@gmail.com
```