require('dotenv').config();
const { readInput, inquirerMenu, pause, placesList } = require("./helpers/inquirer");
const Searches = require("./models/searches");

const main = async () => {

    const searches = new Searches();

    let opt;
    do {
        opt = await inquirerMenu();

        switch (opt) {
            case 1:
                // Show Message
                const city = await readInput('City: ');
                // Search places
                const places = await searches.city(city);
                // Select a place
                const id = await placesList(places);
                if (id === '0') continue;
                const placeSelected = places.find( place => place.id === id);
                // save history
                searches.insertHistory(placeSelected.name);
                // weather
                const weather = await searches.weather(placeSelected.lat, placeSelected.lng);
                // Results

                console.log('\nInformation of the city'.green);
                console.log('City: ', placeSelected.name.green);
                console.log('Lat: ', placeSelected.lat);
                console.log('Lng: ', placeSelected.lng);
                console.log('Temp: ', weather.temp);
                console.log('Min: ', weather.min);
                console.log('Max: ', weather.max);
                console.log('Weather description: ', weather.desc.green);
                break;

            case 2:
                searches.historicalCaptitalizado.forEach( (place, i) => {
                    const idx = `${ i + 1}.`.green;
                    console.log(`${ idx } ${ place }`);
                }); 
                break;
        
            case 0:
                   
                break;
                
        }

        if (opt !== 0) await pause();

    } while (opt != 0);

}

main();